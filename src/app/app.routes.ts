import { RouterModule, Routes } from '@angular/router';

import {
        HomeComponent,
        WishlistComponent,
        CartComponent,
        CheckoutComponent,
        SingleItemComponent,
        ProductsComponent,
        Left3colComponent,
        CategoriesComponent,
        CategoryComponent,
        RegisterComponent,
        AddressesComponent,
        AccountComponent,
        OrdersComponent,
        OrderTrackingComponent,
        DeliveryInfoComponent,
        AboutComponent,
        ContactsComponent,
        SupportComponent,
        PostComponent,
        PostsComponent
        } from './components/index.pages';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'wishlist/:usuario', component: WishlistComponent },
  { path: 'cart/:usuario', component: CartComponent },
  { path: 'checkout/:usuario', component: CheckoutComponent },
  { path: 'filter-product/:termino', component: Left3colComponent },
  { path: 'produtos', component: ProductsComponent },
  { path: 'item/:id', component: SingleItemComponent },
  { path: 'categorias', component: CategoriesComponent },
  { path: 'categoria/:id', component: CategoryComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'conta/personal_info', component: AccountComponent },
  { path: 'conta/addresses', component: AddressesComponent },
  { path: 'conta/orders_history', component: OrdersComponent },
  { path: 'orders_tracking', component: OrderTrackingComponent },
  { path: 'delivery_info', component: DeliveryInfoComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contatos', component: ContactsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

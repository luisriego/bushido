import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; // Solición encontrada para que funcionen los sliders

@Component({
  selector: 'app-master-slider',
  templateUrl: './master-slider.component.html',
  styleUrls: ['./master-slider.component.css']
})
export class MasterSliderComponent implements OnInit {

  visible: string;

  constructor(private router: Router) {      // Solición encontrada para que funcionen los sliders
    router.events.subscribe((url:any) => {   // Solición encontrada para que funcionen los sliders
        if (router.url === '/home') {        // Solición encontrada para que funcionen los sliders
          this.visible = "";                 // Solición encontrada para que funcionen los sliders
        }else{                               // Solición encontrada para que funcionen los sliders
          this.visible = "hidden";           // Solición encontrada para que funcionen los sliders
        }                                    // Solición encontrada para que funcionen los sliders
    })                                       // Solición encontrada para que funcionen los sliders
  }

  ngOnInit() { }

}

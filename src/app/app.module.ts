import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Routes
import { APP_ROUTING } from './app.routes';

// Services
import { SliderService } from "./services/slider.service";

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { LoginModalComponent } from './components/common/login-modal/login-modal.component';
import { NewsComponent } from './components/common/news/news.component';
import { Left3colComponent } from './components/products/left3col/left3col.component';
import { SingleItemComponent } from './components/products/single-item/single-item.component';
import { WishlistComponent } from './components/functions/wishlist/wishlist.component';
import { CartComponent } from './components/functions/cart/cart.component';
import { CheckoutComponent } from './components/functions/checkout/checkout.component';
import { RegisterComponent } from './components/functions/register/register.component';
import { AccountComponent } from './components/functions/account/account.component';
import { AddressesComponent } from './components/functions/addresses/addresses.component';
import { OrdersComponent } from './components/functions/orders/orders.component';
import { OrderTrackingComponent } from './components/functions/order-tracking/order-tracking.component';
import { DeliveryInfoComponent } from './components/functions/delivery-info/delivery-info.component';
import { AboutComponent } from './components/static/about/about.component';
import { ContactsComponent } from './components/static/contacts/contacts.component';
import { SupportComponent } from './components/static/support/support.component';
import { PostsComponent } from './components/blog/posts/posts.component';
import { PostComponent } from './components/blog/post/post.component';
import { ProductComponent } from './components/products/product/product.component';
import { CategoryComponent } from './components/products/category/category.component';
import { Filter2colComponent } from './components/products/filter2col/filter2col.component';
import { CategoriesComponent } from './components/products/categories/categories.component';
import { MasterSliderComponent } from './components/common/master-slider/master-slider.component';
import { SmallSliderComponent } from './components/common/small-slider/small-slider.component';
import { StickyBtnsComponent } from './components/common/sticky-btns/sticky-btns.component';
import { ProductsComponent } from './components/products/products/products.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginModalComponent,
    NewsComponent,
    Left3colComponent,
    SingleItemComponent,
    WishlistComponent,
    CartComponent,
    CheckoutComponent,
    RegisterComponent,
    AccountComponent,
    AddressesComponent,
    OrdersComponent,
    OrderTrackingComponent,
    DeliveryInfoComponent,
    AboutComponent,
    ContactsComponent,
    SupportComponent,
    PostsComponent,
    PostComponent,
    ProductComponent,
    CategoryComponent,
    Filter2colComponent,
    CategoriesComponent,
    MasterSliderComponent,
    SmallSliderComponent,
    StickyBtnsComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [
    SliderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
